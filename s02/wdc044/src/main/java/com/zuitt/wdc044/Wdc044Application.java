package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

// "@SpringBootApplication" is an example of "Annotations" mark.
// Annotations are used to provide supplemental information about the program.
// These are used to manage and configure the behavior of the framework.
// Annotations are used extensively in Spring Boot to configure components, define dependencies, or enable specific functionalities.
// Spring Boot scans for classes in its class path upon running and assigns behaviors on them based on their annotations.

// This specifies the main class of the springboot application.
@SpringBootApplication
// This will indicate that a class is a controller that will handle RESTful web request and returns an HTTP response.
@RestController
public class Wdc044Application {

	public static void main(String[] args) {
		// This method starts the whole Spring Framework.
		// This serves as the entry point to start the application.
		SpringApplication.run(Wdc044Application.class, args);
	}

	// This is used to map HTTP GET requests.
	// Note: HTTP Request annotations is usually followed by a method or class "body".
	// The "method body" contains the logic
	@GetMapping("/hello")
	// "@RequestParam" is used to extract query parameters, form parameters, and even files from the request.
	// If the URL is: /hello?name=john.
	// If the URL is: /hello, the method will return "Hello World".
	// "?" means that the start of a parameters.
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
		String message = String.format("Hello %s!", name);
		// To send a response of "Hello + name".
		return message;
	}

	@GetMapping("/hi")
	public String hi(@RequestParam(value = "user", defaultValue = "user") String user) {
		return "Hi " + user + "!";
	}

	@GetMapping("/nameAge")
	public String nameAge(@RequestParam Map<String, String> requestParams) {
		String name = requestParams.get("name") == null ? "user" : requestParams.get("name");
		String age = requestParams.get("age") == null ? "not sure how many" : requestParams.get("age");

		return String.format("Hello my name is %s and I'm %s years old.", name, age);
	}
}
