package com.zuitt.wdc044.repositories;

import com.zuitt.wdc044.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

// An interface contains behavior that a class implements.
// An interface marked as @Repository contains methods for database manipulation.
// By extending the CrudRepository, PostRepository has inherited its pre-defined methods for creating, retrieving,
// updating and deleting.
public interface PostRepository extends CrudRepository<Post, Object> {

}
