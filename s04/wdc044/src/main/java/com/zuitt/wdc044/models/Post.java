package com.zuitt.wdc044.models;

import javax.persistence.*;

// Mark this Java object as a representation of a database table via @Entity.
@Entity
// Designate table via @Table.
@Table(name = "posts")
public class Post {
    // Indicates that this property represents the primary key via @Id.
    @Id
    // Value for this property will be auto-incremented.
    @GeneratedValue
    private Long id;

    // Class property that represents table columns in a relational database are annotated as @Column.
    @Column
    private String title;

    @Column
    private String content;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    // Default constructor:
    public Post() {}

    public Post(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
